<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     * @param $amount
     * @return array
     */
    public function findCashPayment($amount)
    {
        $array = array(
            '1' => 0,
            '2' => 0,
            '5' => 0,
            '10' => 0,
            '20' => 0,
            '50' => 0,
            '100' => 0
        );

        while ($amount > 0) {
            if ($amount >= 100) {
                $array['100']++;
                $amount = $amount - 100;
            } else if ($amount >= 50) {
                $array['50']++;
                $amount = $amount - 50;
            } else if ($amount >= 20) {
                $array['20']++;
                $amount = $amount - 20;
            } else if ($amount >= 10) {
                $array['10']++;
                $amount = $amount - 10;
            } else if ($amount >= 5) {
                $array['5']++;
                $amount = $amount - 5;
            } else if ($amount >= 2) {
                $array['2']++;
                $amount = $amount - 2;
            } else if ($amount >= 1) {
                $array['1']++;
                $amount = $amount - 1;
            }

        }
        return $array;

    }
}