<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter" => $this->findMedianLetter($this->parseFile($filePath), $occurrences), "count" => $occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     * @return mixed|string
     */
    private function parseFile($filePath)
    {
        // make the content of the file lowercase for easier search as the test
        // don't want there to be a difference between upper and lower case letters
        $parsedFile = strtolower(file_get_contents($filePath));

        // removes all non UTF-8 characters from the file content
        $parsedFile = str_replace("?", "", (utf8_decode($parsedFile)));

        // return the content of the parsed file
        return $parsedFile;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     * @return mixed
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $array = array(); //initializing array for letters and count
        $regex = "/\p{L}/"; //regex to find printable latin characters

        /**
         * While loop to go thought the characters in the file,
         * and remove the ones already covered, and add them to
         * an array of characters if they match the the
         * printable regex for latin characters
         */
        while (strlen($parsedFile) > 0) {
            $letter = substr($parsedFile, 0, 1);
            $count = substr_count($parsedFile, $letter);
            $parsedFile = str_replace($letter, "", $parsedFile);
            if ($count != 0 && preg_match($regex, $letter)) {
                $arr[$letter] = $count;
                array_push($array, array('letter' => $letter, 'count' => $count));
            }
        }


        $columns = array_column($array, 'count'); //array column for sorting
        array_multisort($columns, SORT_DESC, $array); //Sort the array by count, here its descending
        $returnValue = $array[count($array) / 2]; // get the middle result from the array
        $occurrences = $returnValue['count']; //declares the reference parameter
        return $returnValue['letter']; // returns the letter
    }
}